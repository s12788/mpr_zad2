package pl.edu.pjatk.main;



import pl.edu.pjatk.db.*;
import pl.edu.pjatk.domain.*;

public class Test {

	public static void main(String[] args) {
		
				
		AdderToDb adtdb = new AdderToDb();
		
		User user1 = new User();
		User user2 = new User();
		User user3 = new User();
		
		adtdb.addUser(user1, "Mic", "123");
		adtdb.addUser(user2, "Mar", "321");
		adtdb.addUser(user3, "Tulek", "454");
		
		Person person1 = new Person();
		Person person2 = new Person();
		Person person3 = new Person();
		
		adtdb.addPerson(person1, 1, "Michal", "Michalski", user1);
		adtdb.addPerson(person2, 2, "Marek", "Marecki", user2);
		adtdb.addPerson(person3, 3, "Mieszko", "Tulecki", user3);
		
		Address address1 = new Address();
		Address address2 = new Address();
		Address address3 = new Address();
		
		adtdb.addAddress(address1, "Polska", "Gda�sk", "80-337", "Grunwaldzka", "235a", "2", person1);
		adtdb.addAddress(address2, "Polska", "Gda�sk", "80-234", "Havla", "15", "8", person1);
		adtdb.addAddress(address3, "Polska", "Gda�sk", "80-111", "Krzemowa", "3", "2", person3);
		
		
		Permission permission1 = new Permission();
		Permission permission2 = new Permission();
		Permission permission3 = new Permission();
		
		adtdb.addPermission(permission1, "Admin Level");
		adtdb.addPermission(permission2, "User Level 1");
		adtdb.addPermission(permission3, "User Level 2");
		
		
		Role admin = new Role();
		Role user = new Role();
		
		adtdb.addRole (admin, "admin");
		adtdb.addRole (user, "user");
				
		
		
		for (User c: adtdb.getuMng().getAll()){
			System.out.println(c.getId()+" "+c.getLogin()+" "+c.getPassword());
		}
		
		System.out.println("");
		
		for (Person c: adtdb.getpMng().getAll()){
			System.out.println(c.getId()+" "+c.getName()+" "+c.getSurname()
					+" "+c.getUser().getLogin());
		}
		
		System.out.println("");
			
		for (Address c: adtdb.getaMng().getAll()){
		System.out.println(c.getId()+" "+c.getCountry()+" "+c.getPlace()
				+" "+c.getCode()+" "+c.getStreet()+" "+c.getHouseNumber()
				+"/"+c.getLocalNumber()+" "+c.getPerson().getId());
		
		}
		
		System.out.println("");
		
		for (Role c: adtdb.getrMng().getAll()){
		System.out.println(c.getId()+" "+c.getName());
		}
	

	
	}	

}
