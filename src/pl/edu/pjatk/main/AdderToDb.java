package pl.edu.pjatk.main;

import pl.edu.pjatk.db.*;
import pl.edu.pjatk.domain.*;

public class AdderToDb {
	
	private UserManager uMng = new UserManager();
	private PersonManager pMng = new PersonManager();
	private AddressManager aMng = new AddressManager();
	private RoleManager rMng = new RoleManager();
	private PermissionManager perMng = new PermissionManager();
	
	
	void addUser (User user, String login, String passwd) {
		
		user.setLogin(login);
		user.setPassword(passwd);
		uMng.add(user);
		
	}
	
	void addPerson (Person person, int id, String name, String surname, User user) {
		
		person.setId(id);
		person.setName(name);
		person.setSurname(surname);
		person.setUser(user);
		pMng.add(person);
	}
	
	void addAddress (Address address, String country, String place, String code, String street,
			 String houseNumber, String localNumber, Person person){
		address.setCountry(country);
		address.setPlace(place);
		address.setCode(code);
		address.setStreet(street);
		address.setHouseNumber(houseNumber);
		address.setLocalNumber(localNumber);
		address.setPerson(person);
//		person.getAddress().add(address);
		aMng.add(address);
	}
	
	void addRole (Role role, String name) {
		
		role.setName(name);
		rMng.add(role);
	}
	
	void addPermission (Permission permission, String perm){
		permission.setPermission(perm);
		perMng.add(permission);
	}
	

	public UserManager getuMng() {
		return uMng;
	}

	public void setuMng(UserManager uMng) {
		this.uMng = uMng;
	}

	public PersonManager getpMng() {
		return pMng;
	}

	public void setpMng(PersonManager pMng) {
		this.pMng = pMng;
	}

	public AddressManager getaMng() {
		return aMng;
	}

	public void setaMng(AddressManager aMng) {
		this.aMng = aMng;
	}

	public RoleManager getrMng() {
		return rMng;
	}

	public void setrMng(RoleManager rMng) {
		this.rMng = rMng;
	}

	public PermissionManager getPerMng() {
		return perMng;
	}

	public void setPerMng(PermissionManager perMng) {
		this.perMng = perMng;
	}

	
}
